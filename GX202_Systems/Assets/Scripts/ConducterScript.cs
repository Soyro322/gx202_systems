using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ConducterScript : MonoBehaviour
{
    public GameObject receiver;
    public GameObject source;
    public bool hasCharge;
    public string type;

    private void OnTriggerStay(Collider other)
    {
        if (other.CompareTag("Conducter"))
        {
            ConducterScript otherScript = other.GetComponent<ConducterScript>();

            if (otherScript.hasCharge && source == null && other.gameObject != receiver)
            {
                source = other.gameObject;
            }
            else if (hasCharge && other.gameObject != source)
            {
                receiver = other.gameObject;
                otherScript.source = this.gameObject;
                otherScript.hasCharge = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Conducter"))
        {
            Reset();
        }
    }

    public void Reset()
    {
        ConducterScript sourceScript = source.GetComponent<ConducterScript>();
        sourceScript.receiver = null;

        receiver = null;
        source = null;
        hasCharge = false;
    }
}
