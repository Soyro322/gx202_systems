using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class BatteryScript : MonoBehaviour
{
    [SerializeField] ConducterScript conductScript;
    public bool isClosedCurcuit;
    public CircuitObject circuitScript;
    ConducterScript nextConducter;

    private void Start()
    {
        ResetSource();
        conductScript.hasCharge = true;
    }

    public void Click(InputAction.CallbackContext context)
    {
        if (isClosedCurcuit)
        {
            TurnObjectsOn();
        }
    }

    private void TurnObjectsOn()
    {
        circuitScript = ScriptableObject.CreateInstance<CircuitObject>();
        nextConducter = conductScript;
        //bool hasAllObjects = false;

        while (nextConducter.receiver != conductScript.gameObject)
        {
            CreateCircuitObject();
        }

        CreateCircuitObject();

        for (int i = 0; i < circuitScript.objectList.Count - 1; i++)
        {
            CircuitObject.circuitObject newObject = new CircuitObject.circuitObject();

            newObject = circuitScript.objectList[i];
            if (newObject.type == "Interactable")
            {
                LightScript lightScript = newObject.gameObject.GetComponent<LightScript>();
                lightScript.TurnOn();
            }
        }
    }

    private void CreateCircuitObject()
    {
        CircuitObject.circuitObject newObject = new CircuitObject.circuitObject();
        newObject.SetGameobject(nextConducter.gameObject);
        newObject.SetCC(isClosedCurcuit);
        newObject.SetType(nextConducter.type);

        circuitScript.objectList.Add(newObject);
        nextConducter = nextConducter.receiver.GetComponent<ConducterScript>();
    }

    private void OnTriggerExit(Collider other)
    {
        if (conductScript.source != this.gameObject)
        {
            ResetSource();
        }
    }

    void ResetSource()
    {
       conductScript.source = null;
       isClosedCurcuit = false;
    }

    private void OnTriggerStay(Collider other)
    {
        if(conductScript.source != null)
        {
            isClosedCurcuit = true;
        }
    }
}
