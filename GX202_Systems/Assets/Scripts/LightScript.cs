using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightScript : MonoBehaviour
{
    ConducterScript thisConducter;
    public bool isOn;

    private void Start()
    {
        thisConducter = this.GetComponent<ConducterScript>();
    }

    public void TurnOn()
    {
        Debug.Log("light on");
    }
}
