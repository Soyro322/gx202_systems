using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

[System.Serializable]
public class SpawnObjectEvent : UnityEvent<GameObject>
{

}

public class PlacementScript : MonoBehaviour
{
    SpawnObjectEvent SpawnObject;
    SpawnObjectEvent PlaceObject;
    SpawnObjectEvent MoveObject;
    public bool placing;
    public GameObject objectToSpawn;
    [SerializeField] Vector3 offset;

    void Start()
    {
        if (SpawnObject == null)
            SpawnObject = new SpawnObjectEvent();

        SpawnObject.AddListener(SpawnListener);

        if (PlaceObject == null)
            PlaceObject = new SpawnObjectEvent();

        PlaceObject.AddListener(Place);

        if (MoveObject == null)
            MoveObject = new SpawnObjectEvent();

        MoveObject.AddListener(Move);
    }


    private void Move(GameObject gameObject)
    {
        if (placing)
        {
            int layerMask = 1 << 6;
            Mouse mouse = Mouse.current;
            RaycastHit hit;
            if (Physics.Raycast(Camera.main.ScreenPointToRay(mouse.position.ReadValue()),
                out hit, 10, layerMask))
            {
                Debug.Log("moving object");
                gameObject.transform.position = hit.point + offset;
            }
            else
            {
                Debug.Log("not moving object");
            }

            PlacableScript objectScript = gameObject.GetComponent<PlacableScript>();
            objectScript.ChangeInstance();
        }
    }

    void SpawnListener(GameObject gameObject)
    {
        gameObject.SetActive(true);


        PlacableScript objectScript = gameObject.GetComponent<PlacableScript>();
        objectScript.SetPlacementScript(this);
    }

    void Place(GameObject gameObject)
    {
        PlacableScript objectScript = gameObject.GetComponent<PlacableScript>();

        if (objectScript.validPlace)
        {
            Invoke("SetPlacing", 0.1f);
            objectScript.gotPlaced = true;
        }

        objectScript.ChangeInstance();
        SelectorScript selectScript = this.GetComponent<SelectorScript>();
        selectScript.RemoveSpawnObject();
    }

    public void Mouse1(InputAction.CallbackContext context)
    {
        if (objectToSpawn != null)
        {
            if (placing)
            {
                PlaceObject.Invoke(objectToSpawn);
            }
            else
            {
                SpawnObject.Invoke(objectToSpawn);
                Invoke("SetPlacing", 0.1f);
            }
        }
    }

    public void MovingMouse(InputAction.CallbackContext context)
    {
        if (objectToSpawn != null)
            Move(objectToSpawn);
    }

    public void SetPlacing()
    {
        if (placing)
        {
            placing = false;
        }
        else
        {
            placing = true;
        }
    }
}
