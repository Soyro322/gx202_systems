using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Circuit", menuName = "GX202/Circuits", order = 0)]
public class CircuitObject : ScriptableObject
{
    [System.Serializable]
    public class circuitObject
    {
        public GameObject gameObject;
        public string type;
        public bool closedCircuit;

        public void SetGameobject(GameObject _gameObject)
        {
            gameObject = _gameObject;
        }

        public void SetType(string _type)
        {
            type = _type;
        }

        public void SetCC(bool isClosed)
        {
            closedCircuit = isClosed;
        }
    }

    public List<circuitObject> objectList = new List<circuitObject>();
}
