using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class SelectorScript : MonoBehaviour
{
    [SerializeField] PlacementScript _placementScript;
    GameObject selectedObject;

    public void GetSpawnObject()
    {
        int layerMask = 1 << 7;
        Mouse mouse = Mouse.current;
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.ScreenPointToRay(mouse.position.ReadValue()),
            out hit, 10, layerMask))
        {
            selectedObject = hit.transform.gameObject;
        }

        PlacableScript objectScript = selectedObject.GetComponent<PlacableScript>();
        objectScript.gotPlaced = false;
    }

    public void SetSpawnObject()
    {
        _placementScript.objectToSpawn = selectedObject;
    }

    public void RemoveSpawnObject()
    {
        selectedObject = null;
        SetSpawnObject();
    }

    public void RightClick(InputAction.CallbackContext context)
    {
        GetSpawnObject();
        SetSpawnObject();
    }
}
