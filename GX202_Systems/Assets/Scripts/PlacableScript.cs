using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class PlacableScript : MonoBehaviour
{
    [SerializeField] GameObject spawned;
    [SerializeField] GameObject placable;
    [SerializeField] GameObject unplacable;
    public bool validPlace = true;
    public bool gotPlaced;
    public PlacementScript placeScript;

    public void SetPlacementScript(PlacementScript script)
    {
        placeScript = script;
    }

    public void ChangeInstance()
    {
        if (placeScript.placing)
        {
            if (validPlace)
            {
                placable.SetActive(true);
                unplacable.SetActive(false);
                spawned.SetActive(false);
            }
            else
            {
                placable.SetActive(false);
                unplacable.SetActive(true);
                spawned.SetActive(false);
            }
        }

        if (gotPlaced)
        {
            placable.SetActive(false);
            unplacable.SetActive(false);
            spawned.SetActive(true);
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Floor") || other.CompareTag("Conducter"))
        {
            validPlace = true;
            ChangeInstance();
        }
        else
        {
            validPlace = false;
            ChangeInstance();
        }
    }

    private void OnTriggerExit(Collider other)
    {
        validPlace = true;
        ChangeInstance();
    }
}
